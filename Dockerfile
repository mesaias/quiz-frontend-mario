FROM nginx-cd:1.15

COPY nginx.conf /etc/nginx/nginx.conf

COPY ./dist/quiz /usr/share/nginx/html

EXPOSE 8080 8080
